//
//  ViewController.h
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (assign, nonatomic) CGFloat kSamplesPerSecond;
@property (strong, nonatomic) NSString* ciDetectorAccuracy;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;

@end

