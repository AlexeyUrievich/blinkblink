//
//  SettingsViewController.h
//  BlinkBlink
//
//  Created by Alex on 04.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "ViewController.h"

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *accuracySwitch;
@property (weak, nonatomic) IBOutlet UILabel *delayPSLabel;
@property (weak, nonatomic) IBOutlet UISlider *delayPSSlider;
- (IBAction)actionSwitchAccuracy:(UISwitch *)sender;
- (IBAction)actionSliderDelayPS:(UISlider *)sender;
- (IBAction)actionCameraOn:(UIButton *)sender;

@property (assign, nonatomic) CGFloat delayPS;
@property (strong, nonatomic) NSString* ciDetectorAccuracy;

@end
