//
//  SettingsViewController.m
//  BlinkBlink
//
//  Created by Alex on 04.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "SettingsViewController.h"
#import "ViewController.h"

@implementation SettingsViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    if (self.accuracySwitch.isOn)
    {
        self.ciDetectorAccuracy = @"CIDetectorAccuracyHigh";
    } else if (!self.accuracySwitch.isOn)
        {
            self.ciDetectorAccuracy = @"CIDetectorAccuracyLow";
        }
    self.delayPS = self.delayPSSlider.value;
    
}

#pragma mark - Actions

- (IBAction)actionSwitchAccuracy:(UISwitch *)sender
{
    if (sender.isOn)
    {
        NSLog(@"actionSwitchAccuracy changed to ON");
        self.ciDetectorAccuracy = @"CIDetectorAccuracyHigh";
    } else
    {
        NSLog(@"actionSwitchAccuracy changed to OFF");
        self.ciDetectorAccuracy = @"CIDetectorAccuracyLow";
    }
}

- (IBAction)actionSliderDelayPS:(UISlider *)sender
{
    
    self.delayPSLabel.text = [NSString stringWithFormat:@"%.2f", sender.value];
    self.delayPS = sender.value;
}

- (IBAction)actionCameraOn:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"CameraSegue" sender:nil];
}

#pragma mark - Segue
- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController* vc = segue.destinationViewController;
    vc.kSamplesPerSecond = self.delayPS;
    vc.ciDetectorAccuracy = self.ciDetectorAccuracy;
}

@end
