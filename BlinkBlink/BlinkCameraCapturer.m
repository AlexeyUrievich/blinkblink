//
//  BlinkCameraCapturer.m
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "BlinkCameraCapturer.h"
#import <AVFoundation/AVFoundation.h>


@interface BlinkCameraCapturer() <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic) dispatch_queue_t videoDataOutputQueue;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, readwrite) double lastSampleTimestamp;

@property (nonatomic, readwrite) float sampleRate;

@end

@implementation BlinkCameraCapturer

-(void)setAVCaptureAtSampleRate:(float)sampleRate withCameraPreviewView:(UIView *)cameraPreviewView withError:(NSError **)error
{
    self.sampleRate = sampleRate; //kSamplesPerSecond
    self.session = nil;
    self.session = [AVCaptureSession new]; // AVCaptureSessionPreset1280x720  AVCaptureSessionPreset640x480
    //self.session.sessionPreset = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? AVCaptureSessionPreset1280x720 : AVCaptureSessionPresetPhoto;
    //self.session.sessionPreset = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? AVCaptureSessionPresetPhoto : AVCaptureSessionPresetPhoto;
    AVCaptureDeviceInput *deviceInput = [self getCaptureDeviceInput:error];
    
    if (*error)
    {
        [self teardownAVCapture];
        return;
    }
    
    if ( [self.session canAddInput:deviceInput] )
    {
        [self.session addInput:deviceInput];
    }
    
    self.videoDataOutput = [AVCaptureVideoDataOutput new];
    
    //self.videoDataOutput.videoSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : @(kCMPixelFormat_32BGRA) };
    self.videoDataOutput.videoSettings = nil;
    self.videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
    
    self.videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    [self.videoDataOutput setSampleBufferDelegate:self queue:self.videoDataOutputQueue];
    
    if ( [self.session canAddOutput:self.videoDataOutput] )
    {
        [self.session addOutput:self.videoDataOutput];
    }
    
    [[self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:YES];
    
    if (cameraPreviewView)
    {
        self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        self.previewLayer.backgroundColor = [[UIColor blackColor] CGColor];
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

        CALayer *rootLayer = cameraPreviewView.layer;
        
        rootLayer.masksToBounds = YES;
        
        self.previewLayer.frame = rootLayer.bounds;
        
        NSLog(@"previewLayer %.2f, %.2f",self.previewLayer.frame.size.width,  self.previewLayer.frame.size.height);
        
        [rootLayer addSublayer:self.previewLayer];
    }
    [self.session startRunning];
}

-(AVCaptureDeviceInput *)getCaptureDeviceInput:(NSError **)error;
{
    AVCaptureDevice *device;
    
    AVCaptureDevicePosition desiredPosition = AVCaptureDevicePositionFront;
    
    for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo])
    {
        if (d.position == desiredPosition)
        {
            device = d;
            break;
        }
    }
    if( nil == device )
    {
        device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    
    return [AVCaptureDeviceInput deviceInputWithDevice:device error:error];
}

- (void)teardownAVCapture
{
    self.videoDataOutput = nil;
    if (self.videoDataOutputQueue)
    {
        self.videoDataOutputQueue = nil;
    }
    
    [self.previewLayer removeFromSuperlayer];
    self.previewLayer = nil;
    
    self.session = nil;
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    if (!self.lastSampleTimestamp)
    {
        self.lastSampleTimestamp = CACurrentMediaTime();
    }
    else
    {
        double now = CACurrentMediaTime();
        double timePassedSinceLastSample = now - self.lastSampleTimestamp;
        
        if (timePassedSinceLastSample < self.sampleRate)
            return;
        self.lastSampleTimestamp = now;
    }
    
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer
                                                      options:(__bridge NSDictionary *)attachments];
    if (attachments)
    {
        CFRelease(attachments);
    }

    [self.delegate imageFromCamera:ciImage];
}

@end
