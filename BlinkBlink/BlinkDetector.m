//
//  BlinkDetector.m
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "BlinkDetector.h"
#import "BlinkCameraCapturer.h"


@interface BlinkDetector () <BlinkCameraCapturerDelegate>

@property (nonatomic, strong) CIDetector *faceDetector;
@property (strong, nonatomic) BlinkCameraCapturer *cameraCapturer;

@end

@implementation BlinkDetector

//if this value is too low it takes a lot of CPU, if too high the effect is bad cause detection is not happening a lot. 0.3 default

//const float kSamplesPerSecond = 0.3;

- (id)init
{
    self = [super init];
    
    if (self) {

        self.cameraCapturer = [BlinkCameraCapturer new];
        self.cameraCapturer.delegate = self;
        
    }
    return self;
}

-(void)startDetection:(NSError **)error
{
    NSLog(@"in BlinkDetector delayPS - %.2f", self.kSamplesPerSecond);
    NSLog(@"%@", self.ciDetectorAccuracy);
   
     if ([self.ciDetectorAccuracy isEqualToString:@"CIDetectorAccuracyHigh"])
     {
         self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace
                                                context:nil
                                                options:@{CIDetectorAccuracy : CIDetectorAccuracyHigh}]; // CIDetectorAccuracyHigh CIDetectorAccuracyLow
        } else if ([self.ciDetectorAccuracy isEqualToString:@"CIDetectorAccuracyLow"])
        {
            self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace
                                                   context:nil
                                                   options:@{CIDetectorAccuracy : CIDetectorAccuracyLow}]; // CIDetectorAccuracyHigh CIDetectorAccuracyLow
        }
    
    [self.cameraCapturer setAVCaptureAtSampleRate:self.kSamplesPerSecond withCameraPreviewView:self.cameraPreviewView withError:error];
}

-(void)stopDetection
{
    [self.cameraCapturer teardownAVCapture];
}

#pragma mark - BlinkCameraCapturerDelegate

-(void)imageFromCamera:(CIImage *)ciImage
{
   
    // Device oriented vertically, home button on the bottom
    
    NSDictionary *detectionOtions = @{ CIDetectorImageOrientation :@(6),
                                       CIDetectorEyeBlink : @YES,
                                       //CIDetectorAccuracy : CIDetectorAccuracyLow
                                       
                                       };
    
    
    
    NSArray *features = [self.faceDetector featuresInImage:ciImage
                                                   options:detectionOtions];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self extractFacialGesturesFromFeatures:features];
    });
}

#pragma mark - Private methods

-(void)extractFacialGesturesFromFeatures:(NSArray *)features
{
    
    for ( CIFaceFeature *faceFeature in features )
    {

        [self.delegate facePosition:faceFeature.bounds andLeftEyePosition:faceFeature.leftEyePosition andRightEyePosition:faceFeature.rightEyePosition andFaceAngle:faceFeature.faceAngle];
        
        //NSLog(@"%.2f, %.2f,%.2f, %.2f", faceFeature.leftEyePosition.x, faceFeature.leftEyePosition.y , faceFeature.rightEyePosition.x, faceFeature.rightEyePosition.y);
        
        if (faceFeature.leftEyeClosed)
        {
            [self.delegate blinkOccured:GestureTypeLeftBlink];
        }
        if (faceFeature.rightEyeClosed)
        {
            [self.delegate blinkOccured:GestureTypeRightBlink];
        }

        if (faceFeature.faceAngle > 5 | faceFeature.faceAngle < -5)
        {
              NSLog(@"hasFaceangel - %.2f", faceFeature.faceAngle);
        }
    }
}

@end
