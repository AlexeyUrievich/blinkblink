//
//  ViewController.m
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "ViewController.h"
#import "BlinkDetector.h"

@interface ViewController () <BlinkDetectorDelegate>

@property (strong, nonatomic) BlinkDetector* blinkDetector;
@property (assign, nonatomic) NSInteger countOfBlinksLeft;
@property (assign, nonatomic) NSInteger countOfBlinksRight;
@property (strong, nonatomic) UIView* faceView;
@property (strong, nonatomic) UIView* leftEyeView;
@property (strong, nonatomic) UIView* rightEyeView;
@property (assign, nonatomic) BOOL blinkDetectionIsOn;
@property (assign, nonatomic) NSInteger timersCount;
@property (strong, nonatomic) NSTimer* timer;
@property (assign, nonatomic) BOOL timerIsInvalidated;
@property (assign, nonatomic) BOOL timerUsed;


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.blinkDetectionIsOn =  YES;
   
    [self startDetector];
}

#pragma mark BlinkDetectorDelegate

-(void)blinkOccured:(GestureType)facialGestureType
{
    if (self.timerIsInvalidated == YES && self.blinkDetectionIsOn == YES)
    {
        
        /*
        if (facialGestureType == GestureTypeLeftBlink || facialGestureType == GestureTypeRightBlink)
        {
            
            NSLog(@"blink detected");
            [self.blinkDetector stopDetection];
            [self cleanScreen];
            [self startAgain];
        }
        */
        
        if (facialGestureType == GestureTypeLeftBlink)
        {
            self.countOfBlinksLeft = self.countOfBlinksLeft+1;
            NSLog(@"left blinks---, %ld",self.countOfBlinksLeft);
            self.rightEyeView.backgroundColor = [UIColor redColor];
            [self.blinkDetector stopDetection];
            [self cleanScreen];
            [self startAgain];
           
        } else if (facialGestureType == GestureTypeRightBlink)
        {
            self.countOfBlinksRight = self.countOfBlinksRight+1;
            NSLog(@"right blinks---, %ld",self.countOfBlinksRight);
            self.leftEyeView.backgroundColor = [UIColor redColor];
            [self.blinkDetector stopDetection];
            [self cleanScreen];
            [self startAgain];
        }
         
    }
}

-(void)facePosition:(CGRect)bounds
 andLeftEyePosition:(CGPoint)leftEyePoint
andRightEyePosition:(CGPoint)rightEyePoint
       andFaceAngle:(CGFloat)faceAngle
{
    [self cleanScreen];

    self.blinkDetectionIsOn = YES;
   
    self.faceView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-bounds.origin.y*0.4444f-bounds.size.height*0.4444f,
                                                             bounds.origin.x*0.44375f,
                                                             bounds.size.height*0.4444f,
                                                             bounds.size.width*0.44375f)];
    self.faceView.layer.borderWidth = 5.f;
    self.faceView.layer.borderColor = [UIColor greenColor].CGColor;
    [self.view addSubview:self.faceView];
    
    self.leftEyeView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width- leftEyePoint.y*0.44444f-10, leftEyePoint.x*0.44375f-10, 20, 20)];
    self.leftEyeView.backgroundColor = [UIColor colorWithRed:111/255.0 green:28/255.0 blue:147/255.0 alpha:0.7];
    [self.view addSubview:self.leftEyeView];
    
    self.rightEyeView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-rightEyePoint.y*0.44375f-10, rightEyePoint.x*0.44444-10, 20, 20)];
    self.rightEyeView.backgroundColor = [UIColor colorWithRed:111/255.0 green:28/255.0 blue:147/255.0 alpha:0.7];
    [self.view addSubview:self.rightEyeView];
    
    CGFloat tooClose = self.view.bounds.size.width - self.faceView.bounds.size.width;
    if (faceAngle > 5 || faceAngle < -5 || tooClose < 60)
    {
        self.faceView.layer.borderColor = [UIColor redColor].CGColor;
        self.blinkDetectionIsOn = NO;
    } else if (self.timerUsed == NO) // if border is green
        {
            self.timerUsed = YES;
            self.timerIsInvalidated = NO;
            self.timersCount = 4;
            self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self
                                                        selector: @selector(timerForStart) userInfo: nil repeats: YES];
        }
}

#pragma mark - Private Methods

-(void)showError:(NSError *)error
{
    NSLog(@"Failed with error %d", (int)[error code]);
    NSLog(@"%@", [error localizedDescription]);
}

- (void) startAgain
{
    
    [self cleanScreen];
    
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"YOU BLINKED!"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* changeSettings = [UIAlertAction actionWithTitle:@"CHANGE SETTINGS"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      NSLog(@"changeSettings");
                                      [self performSegueWithIdentifier:@"SettingsSegue" sender:nil];
                                  }];
    
    [alertController addAction:changeSettings];
    
    UIAlertAction* startAction = [UIAlertAction actionWithTitle:@"START AGAIN"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action)
                                          {
                                              NSLog(@"start again");
                                              
                                              [self startDetector];
                                              
                                          }];
    
    [alertController addAction:startAction];
    
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void) cleanScreen
{
    [self.faceView removeFromSuperview];
    [self.leftEyeView removeFromSuperview];
    [self.rightEyeView removeFromSuperview];
}

- (void) startDetector
{
    
    self.countOfBlinksLeft = 0;
    self.countOfBlinksRight = 0;
    self.timerUsed = NO;
    
    self.blinkDetector = [BlinkDetector new];
    self.blinkDetector.delegate = self;
    self.blinkDetector.kSamplesPerSecond = self.kSamplesPerSecond;
    self.blinkDetector.ciDetectorAccuracy = self.ciDetectorAccuracy;
    self.previewView.frame = self.view.frame; // this is for correct visualisation of video
    self.blinkDetector.cameraPreviewView = self.previewView;
    NSError *error;
    [self.blinkDetector startDetection:&error];
    if (error)
    {
        [self showError:error];
    }
}

- (void) timerForStart
{
    self.timersCount = self.timersCount -1;
    self.timerLabel.text = [NSString stringWithFormat:@"%ld", self.timersCount];
    
    if (self.timersCount == 0) {
        [self.timer invalidate];
        self.timerLabel.text = @"";
        self.timerIsInvalidated = YES;
    }
}

@end
