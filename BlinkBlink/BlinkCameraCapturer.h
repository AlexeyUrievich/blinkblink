//
//  BlinkCameraCapturer.h
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol BlinkCameraCapturerDelegate <NSObject>

-(void)imageFromCamera:(CIImage *)ciImage;

@end

@interface BlinkCameraCapturer : NSObject

@property (nonatomic, weak) id<BlinkCameraCapturerDelegate> delegate;

-(void)setAVCaptureAtSampleRate:(float)sampleRate withCameraPreviewView:(UIView *)cameraPreviewView withError:(NSError **)error;
- (void)teardownAVCapture;

@end
