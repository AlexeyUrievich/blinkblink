//
//  BlinkDetector.h
//  BlinkBlink
//
//  Created by Alex on 01.04.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    GestureTypeSmile,
    GestureTypeLeftBlink,
    GestureTypeRightBlink,
    
} GestureType;

@protocol BlinkDetectorDelegate <NSObject>

-(void)blinkOccured:(GestureType)facialGestureType;
-(void)facePosition:(CGRect)bounds
  andLeftEyePosition:(CGPoint)leftEyePoint
andRightEyePosition:(CGPoint)rightEyePoint
       andFaceAngle:(CGFloat)faceAngle;

@end

@interface BlinkDetector : NSObject

@property (nonatomic, weak) id<BlinkDetectorDelegate> delegate;
@property (assign, nonatomic) CGFloat kSamplesPerSecond;
@property (strong, nonatomic) NSString* ciDetectorAccuracy;

@property (weak, nonatomic) UIView *cameraPreviewView;

-(void)startDetection:(NSError **)error;
-(void)stopDetection;

@end


